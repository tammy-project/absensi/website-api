<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $this->load->model('User_model', 'user');
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$check = $this->user->find_by('username',$username, false);
		if ($check->num_rows() == 1) {
            $user_data = $check->row();
            $verify_password = password_verify($password, $user_data->password);
            if ($verify_password) {
                $output['status'] = true;
                $output['message'] = 'Selamat Datang '.$user_data->nama;
                $output['user'] = $user_data; 
            } else {
            	$output['status'] = false;
            	$output['message'] = 'Password tidak sesuai'; 
            }
        } else {
        	$output['status'] = false;
            $output['message'] = 'User tidak ditemukan';
        }
        echo json_encode($output);
	}

}

/* End of file api_Auth.php */
/* Location: ./application/controllers/api/api_Auth.php */
