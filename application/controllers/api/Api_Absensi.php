<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Absensi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Absensi_model', 'absensi');
		$this->load->model('Karyawan_model', 'karyawan');
		$this->load->model('Jam_model', 'jam');
		$this->load->helper('Tanggal');
		$this->load->model('Shift_model', 'shift');
	}

	function daftarKaryawan()
	{
		echo json_encode($this->karyawan->get_all());
	}

	function detailAbsen()
	{
		$id_user = $this->input->get('id_user');
    	$bulan = @$this->input->get('bulan') ? $this->input->get('bulan') : date('m');
    	$tahun = @$this->input->get('tahun') ? $this->input->get('tahun') : date('Y');

    	$data['karyawan'] = $this->karyawan->find($id_user);
    	$data['absen'] = $this->absensi->get_absen($id_user, $bulan, $tahun);
    	$data['jam_kerja'] = $this->shift->get_by_user($id_user);
    	$data['all_bulan'] = bulan();
    	$data['bulan'] = $bulan;
    	$data['tahun'] = $tahun;
    	$data['hari'] = hari_bulan($bulan, $tahun);

    	if ($data['absen']) {
    		foreach ($data['hari'] as $i => $h) {
	    		$absen_harian = array_search($h['tgl'], array_column($data['absen'], 'tgl')) !== false ? $data['absen'][array_search($h['tgl'], array_column($data['absen'], 'tgl'))] : '';
	    		$data['hari'][$i]['weekend'] = in_array($h['hari'], ['Minggu']);
	    		$data['hari'][$i]['jam_masuk'] = is_weekend($h['tgl']) ? ['status' => 'libur','text' => 'Libur Akhir Pekan'] :  check_wkatu_absensi(@$absen_harian['jam_masuk'], 'Masuk', $data['karyawan']->shift_id, true);
				$data['hari'][$i]['lokasi_masuk'] = empty($absen_harian['lokasi_absen_masuk']) ? '' : $absen_harian['lokasi_absen_masuk'];
				$data['hari'][$i]['jam_pulang'] = is_weekend($h['tgl']) ? ['status' => 'libur','text' => 'Libur Akhir Pekan'] : check_wkatu_absensi(@$absen_harian['jam_pulang'], 'Pulang', $data['karyawan']->shift_id, true);
				$data['hari'][$i]['lokasi_pulang'] = empty($absen_harian['lokasi_absen_pulang']) ? '' : $absen_harian['lokasi_absen_pulang'];
	    	}
    	}else{
    		$data['hari'] = [];
    	}
 
    	// header('Content-Type: application/json');
    	echo json_encode($data);
	}

	function absenPengguna()
	{
		$absen_harian = $this->absensi->absen_harian_user($this->input->post('id_user'))->num_rows();
		$keterangan = ($absen_harian < 2 && $absen_harian < 1) ? 'Masuk' : 'Pulang';
		
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');

		if((!empty($lat)) || (!empty($lng))){
			if (!empty($this->input->post('userfile'))) {
				$upload =$this->saveBase64ImagePng($this->input->post('userfile'));
				if ($upload['status'] !== false) {
					$resultLocation = $this->checkDistance($lat,$lng);
					if($resultLocation){
						$data = [
							'tgl' => date('Y-m-d'),
							'waktu' => date('H:i:s'),
							'keterangan' => $keterangan,
							'lokasi_absen' =>$resultLocation['location'],
							'id_user' => $this->input->post('id_user'),
							'foto_url' => $upload['name']
						];
						$result = $this->absensi->insert_data($data);
						if ($result) {
							$response = array(
								'status' => true,
								'message' => 'Absensi berhasil dicatat'
							);
						} else {
							$response = array(
								'status' => false,
								'message' => 'Absensi gagal dicatat'
							);
						}
					}else{
						$response = array(
							'status' => false,
							'message' => 'Karyawan berada di luar titik yang ditentukan'
						);
					}
				}else{
					$response = array(
						'status' => false,
						'message' =>  $this->upload->display_errors()
					);		
				}
			}else{
				$response = array(
					'status' => false,
					'message' => 'Gagal mengambil foto' 
				);	
			}	
		}else{
			$response = array(
				'status' => false,
				'message' => 'Terjadi kesalahan mengambil posisi karyawan' 
			);
		}
		echo json_encode($response);
	}

	function checkAbsen()
	{
		$id_user = $this->input->post('id_user');
		$absen = $this->absensi->check_absen($id_user, date('d'),date('m'), date('Y'));
		if (is_weekend(date('Y-m-d'))) {
			$response = array(
				'weekend' => true
			);
		}else{
			$response = array(
				'weekend' => false
			);
			$response['absen_masuk'] = !empty($absen->jam_masuk);
			$response['absen_pulang'] = !empty($absen->jam_pulang);
		}
		$response['tanggal'] = date("d-m-Y");
 		$response['karyawan'] = $this->karyawan->find($id_user);
		echo json_encode($response);
	}

}

/* End of file Api_Absensi.php */
/* Location: ./application/controllers/api/Api_Absensi.php */
