<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Shift extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Shift_model', 'shift');
	}

	function getAll()
	{
		 echo json_encode($this->shift->get_all());
	}

	function store()
	{
		 $data = array(
        	'nama_shift' 	=> $this->input->post('nama_shift'),
        	'jam_mulai'		=> $this->input->post('jam_mulai'),
        	'jam_selesai'	=> $this->input->post('jam_selesai') 
        );
        $result = $this->shift->insert_data($data);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Shift berhasil ditambahkan!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Shift gagal ditambahkan!'
            ];
        }

        echo json_encode($response);
	}

	function update()
    {
        $data = array(
        	'nama_shift' 	=> $this->input->post('nama_shift'),
        	'jam_mulai'		=> $this->input->post('jam_mulai'),
        	'jam_selesai'	=> $this->input->post('jam_selesai') 
        );

        $result = $this->shift->update_data($this->input->post('id_shift'), $data);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Shift berhasil diupdate!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Shift gagal diupdate!'
            ];
        }
        echo json_encode($response);
    }

    function destroy()
    {
    	$id = $this->input->post('id');
        $result = $this->shift->delete_data($id);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Shift telah dihapus!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Shift gagal dihapus!'
            ];
        }
        echo json_encode($response);
    }

}

/* End of file Api_Shift.php */
/* Location: ./application/controllers/api/Api_Shift.php */
