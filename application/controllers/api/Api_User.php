<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('User_model', 'user');
        $this->load->model('Shift_model', 'shift');
        $this->load->model('Divisi_model', 'divisi');
	}

	function getUser()
	{
		$id_user=$this->input->post('id_user');
		 echo json_encode($this->user->find_by('id_user', $id_user, true));
	}

	function update()
    {
    	$id_user=$this->input->post('id_user');
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'email' => $post['email'],
            'username' => $post['username'],
        ];
        $password = $this->input->post('password');


        if (!empty($password)) {
            $data['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        }

        $result = $this->user->update_data($id_user, $data);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Profil berhasil diubah!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Profil gagal diubah!'
            ];
        }

        echo json_encode($response);
    }

}

/* End of file Api_User.php */
/* Location: ./application/controllers/api/Api_User.php */
