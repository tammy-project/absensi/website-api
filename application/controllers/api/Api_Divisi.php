<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Divisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Divisi_model', 'divisi');
	}

	function getAll()
	{
		 echo json_encode($this->divisi->get_all());
	}

	function store()
	{
		$nama_divisi = $this->input->post('nama_divisi');
        $result = $this->divisi->insert_data(['nama_divisi' => $nama_divisi]);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Divisi berhasil ditambahkan!',
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Divisi gagal ditambahkan!'
            ];
        }

        echo json_encode($response);
	}

	function update()
    {
        $id_divisi = $this->input->post('id_divisi');
        $nama_divisi = $this->input->post('nama_divisi');

        $result = $this->divisi->update_data($id_divisi, ['nama_divisi' => $nama_divisi]);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Divisi berhasil diupdate!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Divisi gagal diupdate!'
            ];
        }
        echo json_encode($response);
    }

    function destroy()
    {
    	$id = $this->input->post('id');
        $result = $this->divisi->delete_data($id);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Divisi telah dihapus!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Divisi gagal dihapus!'
            ];
        }
        echo json_encode($response);
    }

}

/* End of file Api_Divisi.php */
/* Location: ./application/controllers/api/Api_Divisi.php */
