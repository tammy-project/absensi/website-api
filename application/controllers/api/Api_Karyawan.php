<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Karyawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Karyawan_model', 'karyawan');
        $this->load->model('Shift_model', 'shift');
        $this->load->model('Divisi_model', 'divisi');
	}

	function getAll()
	{
		 echo json_encode($this->karyawan->get_all());
	}

	function store()
	{
		$post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'shift_id' => $post['id_shift'],
            'username' => $post['username'],
            'password' => password_hash($post['password'], PASSWORD_DEFAULT),
        ];

        $result = $this->karyawan->insert_data($data);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Data karyawan telah ditambahkan!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Data karyawan gagal ditambahkan'
            ];
        }

        echo json_encode($response);
	}

	function update()
    {
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'shift_id' => $post['id_shift'],
            'username' => $post['username'],
        ];

        if ($post['password'] !== '') {
            $data['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        }

        $result = $this->karyawan->update_data($post['id_user'], $data);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Data Karyawan berhasil diubah!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Data Karyawan gagal diubah!'
            ];
        }

        echo json_encode($response);
    }

    function destroy()
    {
    	$id_user = $this->input->post('id');
        $result = $this->karyawan->delete_data($id_user);
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Data karyawan berhasil dihapus!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Data karyawan gagal dihapus!'
            ];
        }
        echo json_encode($response);
    }

}

/* End of file Api_Karyawan.php */
/* Location: ./application/controllers/api/Api_Karyawan.php */
