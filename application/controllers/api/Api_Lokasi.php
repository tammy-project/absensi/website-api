<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Lokasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lokasi_model', 'lokasi');
	}

	function getAll()
	{
		 echo json_encode($this->lokasi->get_all());
	}

	function store()
	{
		$result = $this->lokasi->insert_data();
		if ($result) {
            $response = [
                'status' => true,
                'message' => 'Lokasi berhasil ditambahkan!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Lokasi gagal ditambahkan!'
            ];
        }

        echo json_encode($response);
	}

	function update()
    {
        $result = $this->lokasi->update_data();
		if ($result) {
            $response = [
                'status' => true,
                'message' => 'Lokasi berhasil diupdate!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Lokasi gagal diupdate!'
            ];
        }
        echo json_encode($response);
    }

    function destroy()
    {
    	$result = $this->lokasi->delete_data($this->input->post('id'));
        if ($result) {
            $response = [
                'status' => true,
                'message' => 'Lokasi telah dihapus!'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Lokasi gagal dihapus!'
            ];
        }
        echo json_encode($response);
    }

}

/* End of file Api_Lokasi.php */
/* Location: ./application/controllers/api/Api_Lokasi.php */
