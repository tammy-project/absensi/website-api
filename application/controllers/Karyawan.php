<?php
defined('BASEPATH') OR die('No direct script access allowed!');

class Karyawan extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        redirect_if_level_not('Manager');
        $this->load->model('Karyawan_model', 'karyawan');
        $this->load->model('Shift_model', 'shift');
        $this->load->model('Divisi_model', 'divisi');
    }

    public function index()
    {
        $data['karyawan'] = $this->karyawan->get_all();
        return $this->template->load('template', 'karyawan/index', $data);
    }

    public function create()
    {
        $data['divisi'] = $this->divisi->get_all();
        $data['shift'] = $this->shift->get_all();
        return $this->template->load('template', 'karyawan/create', $data);
    }

    public function store()
    {
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'shift_id' => $post['id_shift'],
            'username' => $post['username'],
            'password' => password_hash($post['password'], PASSWORD_DEFAULT),
        ];

        $result = $this->karyawan->insert_data($data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data karyawan telah ditambahkan!'
            ];
            $redirect = 'karyawan/';
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data karyawan gagal ditambahkan'
            ];
            $redirect = 'karyawan/create';
        }
        
        $this->session->set_flashdata('response', $response);
        redirect($redirect);
    }

    public function edit()
    {

        $data['divisi'] = $this->divisi->get_all();
        $data['shift'] = $this->shift->get_all();
        $id_user = $this->uri->segment(3);
        $data['karyawan'] = $this->karyawan->find($id_user);
        return $this->template->load('template', 'karyawan/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'shift_id' => $post['id_shift'],
            'username' => $post['username'],
        ];

        if ($post['password'] !== '') {
            $data['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        }

        $result = $this->karyawan->update_data($post['id_user'], $data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data Karyawan berhasil diubah!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data Karyawan gagal diubah!'
            ];
        }
        
        $this->session->set_flashdata('response', $response);
        redirect('karyawan');
    }

    //  public function update_foto()
    // {
    //     $config = [
    //         'upload_path' => './assets/img/profil',
    //         'allowed_types' => 'gif|jpg|png',
    //         'file_name' => round(microtime(date('dY')))
    //     ];

    //     $this->load->library('upload', $config);

    //     if (!$this->upload->do_upload('foto')) {
    //         $response = [
    //             'status' => 'error',
    //             'message' => $this->upload->display_errors()
    //         ];

    //         $this->session->set_flashdata('response', $response);
    //         return redirect('user');
    //     }

    //     $data_foto = $this->upload->data();
    //     $data['foto'] = $data_foto['file_name'];
    //     $result = $this->user->update_data($this->session->id_user, $data);
    //     if ($result) {
    //         $response = [
    //             'status' => 'success',
    //             'message' => 'Foto Profil berhasil diubah!'
    //         ];
    //     } else {
    //         $response = [
    //             'status' => 'error',
    //             'message' => 'Foto Profil gagal diubah!'
    //         ];
    //         unlink($data_foto['full_path']);
    //     }
        
    //     $this->session->set_flashdata('response', $response);
    //     redirect('user');
    // }


    public function destroy()
    {
        $id_user = $this->uri->segment(3);
        $result = $this->karyawan->delete_data($id_user);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data karyawan berhasil dihapus!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data karyawan gagal dihapus!'
            ];
        }
        
        header('Content-Type: application/json');
        echo $response;
    }
}



/* End of File: d:\Ampps\www\project\absen-pegawai\application\controllers\Karyawan.php */
