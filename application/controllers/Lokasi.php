<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_login();
        redirect_if_level_not('Manager');
		$this->load->model('Lokasi_model','lokasi');
	}

	public function index()
	{
		$data['lokasi'] = $this->lokasi->get_all();
        return $this->template->load('template', 'lokasi', $data);
	}

	public function add()
	{
		$result = $this->lokasi->insert_data();
		if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Lokasi berhasil ditambahkan!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Lokasi gagal ditambahkan!'
            ];
        }
        return $this->response_json($response);
	}

	public function update()
	{
		$result = $this->lokasi->update_data();
		if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Lokasi berhasil diupdate!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Lokasi gagal diupdate!'
            ];
        }
        return $this->response_json($response);
	}

	public function delete($id_lokasi)
	{
        $result = $this->lokasi->delete_data($id_lokasi);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Lokasi telah dihapus!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Lokasi gagal dihapus!'
            ];
        }

        return $this->response_json($response);
	}


	private function response_json($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}

/* End of file Lokasi.php */
/* Location: ./application/controllers/Lokasi.php */
