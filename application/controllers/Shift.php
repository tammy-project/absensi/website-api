<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shift extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_login();
        redirect_if_level_not('Manager');
        $this->load->model('Shift_model', 'shift');
    }

    public function index()
    {
        $data['shift'] = $this->shift->get_all();
        return $this->template->load('template', 'shift', $data);
    }

    public function store()
    {
        $data = array(
        	'nama_shift' 	=> $this->input->post('nama_shift'),
        	'jam_mulai'		=> $this->input->post('jam_mulai'),
        	'jam_selesai'	=> $this->input->post('jam_selesai') 
        );
        $result = $this->shift->insert_data($data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Shift berhasil ditambahkan!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Shift gagal ditambahkan!'
            ];
        }

        return $this->response_json($response);
    }

    public function update()
    {
        $data = array(
        	'nama_shift' 	=> $this->input->post('nama_shift'),
        	'jam_mulai'		=> $this->input->post('jam_mulai'),
        	'jam_selesai'	=> $this->input->post('jam_selesai') 
        );

        $result = $this->shift->update_data($this->input->post('id_shift'), $data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Shift berhasil diupdate!',
                'data' => $result
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Shift gagal diupdate!'
            ];
        }

        return $this->response_json($response);
    }

    public function destroy($id='')
    {
        $result = $this->shift->delete_data($id);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Shift telah dihapus!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Shift gagal dihapus!'
            ];
        }

        return $this->response_json($response);
    }

    private function response_json($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
    }




}

/* End of file Shift.php */
/* Location: ./application/controllers/Shift.php */
