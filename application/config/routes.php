<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// API ROUTE
// AUTH
$route['api/auth'] = 'api/Api_Auth/login';

// SHIFT
$route['api/shift/all'] = 'api/Api_Shift/getAll';
$route['api/shift/add'] = 'api/Api_Shift/store';
$route['api/shift/edit'] = 'api/Api_Shift/update';
$route['api/shift/delete'] = 'api/Api_Shift/destroy';

// Karyawan
$route['api/karyawan/all'] = 'api/Api_Karyawan/getAll';
$route['api/karyawan/add'] = 'api/Api_Karyawan/store';
$route['api/karyawan/edit'] = 'api/Api_Karyawan/update';
$route['api/karyawan/delete'] = 'api/Api_Karyawan/destroy';

// DIVISI
$route['api/divisi/all'] = 'api/Api_Divisi/getAll';
$route['api/divisi/add'] = 'api/Api_Divisi/store';
$route['api/divisi/edit'] = 'api/Api_Divisi/update';
$route['api/divisi/delete'] = 'api/Api_Divisi/destroy';

// LOKASI
$route['api/lokasi/all'] = 'api/Api_Lokasi/getAll';
$route['api/lokasi/add'] = 'api/Api_Lokasi/store';
$route['api/lokasi/edit'] = 'api/Api_Lokasi/update';
$route['api/lokasi/delete'] = 'api/Api_Lokasi/destroy';

//PROFIL
$route['api/profil'] = 'api/Api_User/getUser';
$route['api/profil/edit'] = 'api/Api_User/update';

//ABSENSI
$route['api/absensi/karyawan'] = 'api/Api_Absensi/daftarKaryawan';
$route['api/absensi/detail'] = 'api/Api_Absensi/detailAbsen';
$route['api/absensi/absen'] = 'api/Api_Absensi/absenPengguna';
$route['api/absensi/check'] = 'api/Api_Absensi/checkAbsen';
