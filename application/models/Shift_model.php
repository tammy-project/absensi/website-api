<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shift_model extends CI_Model {

	public function get_all()
	{
		return $this->db->get('shift')->result();
	}

	public function find($id)
    {
        $this->db->where('id_shift', $id);
        return $this->db->get('shift')->row();
    }

    public function insert_data($data)
    {
        $result = $this->db->insert('shift', $data);
        if ($result) {
            $new_id = $this->db->insert_id();
            $data = $this->find($new_id);
            return $data;
        } 
        return $result;
    }

    public function update_data($id, $data)
    {
        $this->db->where('id_shift', $id);
        $result = $this->db->update('shift', $data);
        if ($result) {
            $data = $this->find($id);
            return $data;
        } 
        return $result;
    }

    public function delete_data($id)
    {
        $this->db->where('id_shift', $id);
        $result = $this->db->delete('shift');
        return $result;
    }

    public function get_by_user($id_user)
    {
    	$this->db->select('shift.*');
        $this->db->join('users', 'users.shift_id = shift.id_shift');
    	$this->db->where('id_user', $id_user);
    	return $this->db->get('shift')->row_array();
    }

}

/* End of file Shift_model.php */
/* Location: ./application/models/Shift_model.php */
