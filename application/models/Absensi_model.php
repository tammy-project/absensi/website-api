<?php
defined('BASEPATH') OR die('No direct script access allowed!');

class Absensi_model extends CI_Model 
{
    public function get_absen($id_user, $bulan, $tahun)
    {
        $this->db->select("DATE_FORMAT(a.tgl, '%d-%m-%Y') AS tgl, a.waktu AS jam_masuk, al.waktu AS jam_pulang, a.lokasi_absen as lokasi_absen_masuk, al.lokasi_absen as lokasi_absen_pulang, a.foto_url as foto_masuk, al.foto_url as foto_keluar,");
        $this->db->where('a.id_user', $id_user);
        $this->db->where("DATE_FORMAT(a.tgl, '%m') = ", $bulan);
        $this->db->where("DATE_FORMAT(a.tgl, '%Y') = ", $tahun);
        $this->db->where('a.keterangan', 'Masuk');
        $this->db->group_by("tgl");
        $this->db->join('absensi al', 'a.tgl = al.tgl AND al.id_user=\''.$id_user.'\' AND al.keterangan = \'Pulang\'', 'left');
        $result = $this->db->get('absensi a');
        return $result->result_array();
    }

    public function absen_harian_user($id_user)
    {
        $today = date('Y-m-d');
        $this->db->where('tgl', $today);
        $this->db->where('id_user', $id_user);
        $data = $this->db->get('absensi');
        return $data;
    }

    public function insert_data($data)
    {
        $result = $this->db->insert('absensi', $data);
        return $result;
    }

    public function get_jam_by_time($time)
    {
        $this->db->where('start', $time, '<=');
        $this->db->or_where('finish', $time, '>=');
        $data = $this->db->get('jam');
        return $data->row();
    }

    public function check_absen($id_user, $hari, $bulan, $tahun)
    {
    	$this->db->select("DATE_FORMAT(a.tgl, '%d-%m-%Y') AS tgl, a.waktu AS jam_masuk, al.waktu AS jam_pulang, a.lokasi_absen as lokasi_absen_masuk, al.lokasi_absen as lokasi_absen_pulang");
        $this->db->where('a.id_user', $id_user);
        $this->db->where("DATE_FORMAT(a.tgl, '%d') = ", $hari);
        $this->db->where("DATE_FORMAT(a.tgl, '%m') = ", $bulan);
        $this->db->where("DATE_FORMAT(a.tgl, '%Y') = ", $tahun);
        $this->db->where('a.keterangan', 'Masuk');
        $this->db->group_by("tgl");
        $this->db->join('absensi al', 'a.tgl = al.tgl AND al.id_user=\''.$id_user.'\' AND al.keterangan = \'Pulang\'', 'left');
        $result = $this->db->get('absensi a');
        return $result->row();
    }
}



/* End of File: d:\Ampps\www\project\absen-pegawai\application\models\Absensi_model.php */
