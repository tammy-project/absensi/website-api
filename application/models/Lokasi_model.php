<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi_model extends CI_Model {

	function get_all()
    {
        $result = $this->db->get('lokasi');
        return $result->result();
    }

    public function find($id)
    {
        $this->db->where('id_lokasi', $id);
        $result = $this->db->get('lokasi');
        return $result->row();
    }

    function insert_data()
    {
    	$data = array(
    		'nama_lokasi' => $this->input->post('nama_lokasi'),
    		'lng' => $this->input->post('lng'),
    		'lat' => $this->input->post('lat'),
    		'radius' => $this->input->post('radius') 
    	);
    	$result = $this->db->insert('lokasi', $data);

    	if ($result) {
    		$new_id = $this->db->insert_id();
            $data = $this->find($new_id);
            return $data;
    	}
    	return $result;
    }

    function update_data()
    {
    	$data = array();
    	if (!empty($this->input->post('nama_lokasi'))) {
    		$data['nama_lokasi'] = $this->input->post('nama_lokasi');
    	}
    	if (!empty($this->input->post('radius'))) {
    		$data['radius'] = $this->input->post('radius');
    	}
    	if (!empty($this->input->post('lat'))) {
    		$data['lat'] = $this->input->post('lat');
    	}
    	if (!empty($this->input->post('lng'))) {
    		$data['lng'] = $this->input->post('lng');
    	}
    	if (!empty($data)) {
    		$this->db->where('id_lokasi', $this->input->post('id_lokasi'));
    		$result = $this->db->update('lokasi', $data);
	    	if ($result) {
	            $data = $this->find($this->input->post('id_lokasi'));
	            return $data;
	        } 
    	}else{
    		$result = true;
    	}
        return $result;
    }

    function delete_data($id)
    {
    	$this->db->where('id_lokasi', $id);
        $result = $this->db->delete('lokasi');
        return $result;
    }
}

/* End of file Lokasi_model.php */
/* Location: ./application/models/Lokasi_model.php */
