<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title float-left">Shift Karyawan</h4>
				<div class="d-inline ml-auto float-right">
					<a href="#" class="btn btn-success btn-sm btn-add-shift" data-toggle="modal" data-target="#modal-add-shift"><i class="fa fa-plus"></i> Tambah Shift</a>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<th>No.</th>
							<th>Nama Shift</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
							<th>Aksi</th>
						</thead>
						<tbody id="tbody-shift">
							<?php foreach($shift as $i => $d): ?>
								<tr id="<?= 'shift-' . $d->id_shift ?>">
									<td><?= ($i+1) ?></td>
									<td class="nama-shift"><?= $d->nama_shift ?></td>
									<td class="jam-mulai"><?= $d->jam_mulai ?></td>
									<td class="jam-selesai"><?= $d->jam_selesai ?></td>
									<td>
										<a href="#" class="btn btn-primary btn-sm btn-edit-shift" data-toggle="modal" data-target="#modal-edit-shift" data-shift="<?= base64_encode(json_encode($d)) ?>"><i class="fa fa-edit"></i> Edit</a> 
										<a href="<?= base_url('shift/destroy/' . $d->id_shift) ?>" class="btn btn-danger btn-sm btn-delete ml-2" onclick="return false"><i class="fa fa-trash"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-shift" tabindex="-1" role="dialog" aria-labelledby="modal-add-shift-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-add-shift" action="<?= base_url('shift/store') ?>" method="post" onsubmit="return false">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-add-shift-label">Tambah Shift Karyawan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="nama-shift">Nama Shift :</label>
						<input type="text" name="nama_shift" id="nama-shift" class="form-control" placeholder="Nama Shift" required="reuired" />
					</div>
					<div class="form-group">
						<label for="jam-mulai">Jam Mulai :</label>
						<input type="time" name="jam_mulai" id="jam-mulai" class="form-control" placeholder="Jam Mulai" required="reuired" />
					</div>
					<div class="form-group">
						<label for="jam-selesai">Jam Selesai :</label>
						<input type="time" name="jam_selesai" id="jam-selesai" class="form-control" placeholder="Jam Selesai" required="reuired" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-edit-shift" tabindex="-1" role="dialog" aria-labelledby="modal-edit-shift-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-edit-shift" action="<?= base_url('shift/update') ?>" method="post" onsubmit="return false">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-edit-shift-label">Edit Shift Karyawan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="edit-nama-shift">Nama Shift :</label>
						<input type="hidden" name="id_shift" id="edit-id-shift">
						<input type="text" name="nama_shift" id="edit-nama-shift" class="form-control" placeholder="Nama Shift" required="reuired" />
					</div>
					<div class="form-group">
						<label for="edit-jam-mulai">Jam Mulai :</label>
						<input type="time" name="jam_mulai" id="edit-jam-mulai" class="form-control" placeholder="Nama Shift" required="reuired" />
					</div>
					<div class="form-group">
						<label for="edit-jam-selesai">Jam Selesai :</label>
						<input type="time" name="jam_selesai" id="edit-jam-selesai" class="form-control" placeholder="Jam Selesai" required="reuired" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
