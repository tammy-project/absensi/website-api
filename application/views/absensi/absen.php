<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Absen Harian</h4>
            </div>
            <div class="card-body">
                <table class="table w-100">
                    <thead>
                        <th>Status</th>
                        <th>Tanggal</th>
                        <th>Absen Masuk</th>
                        <th>Absen Pulang</th>
                    </thead>
                    <tbody>
                        <tr>
                            <?php if(is_weekend()): ?>
                                <td class="bg-light text-danger" colspan="4">Hari ini libur. Tidak Perlu absen</td>
                            <?php else: ?>
                                <td><i class="fa fa-3x fa-<?= ($absen < 2) ? "warning text-warning" : "check-circle-o text-success" ?>"></i></td>
                                <td><?= tgl_hari(date('d-m-Y')) ?></td>
                                <td>
                                	<?= 
                                		($absen >= 1) ? 
                                		"<button class='btn btn-primary btn-sm btn-fill' disabled style='cursor:not-allowed'>Absen Masuk</button>":
                                		"<button class='btn btn-primary btn-sm btn-fill' data-toggle='modal' data-target='#modalAbsenMasuk' onClick='openWeb()'>Absen Masuk</button>";
                                		 // "<a href='".base_url('absensi/absen/masuk')."' class='btn btn-primary btn-sm btn-fill'>Absen Masuk</a>"
                                    ?>
                                </td>
                                <td>
                                	<?= 
                                		($absen != 1 || $absen == 2) ? 
                                		"<button class='btn btn-success btn-sm btn-fill' disabled style='cursor:not-allowed'>Absen Pulang</button>":
                                		"<button class='btn btn-success btn-sm btn-fill' data-toggle='modal' data-target='#modalAbsenPulang' onClick='openWebPulang()')>Absen Pulang</button>";
                                		// "<a href='".base_url('absensi/absen/pulang')."' class='btn btn-success btn-sm btn-fill'>Absen Pulang</a>"
                                    ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal Absen Masuk -->
<div id="modalAbsenMasuk" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form id="form-absen-masuk" action="<?= base_url('absensi/absen/masuk') ?>" method="post">
	      <div class="modal-header">
	        <h5 class="modal-title">Absen Masuk</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	            <div class="col-md-3">
	                <center>
	                	<div id="my_camera" class="col-md-3"></div>
	                </center>
	                <input type="hidden" name="userfile" class="image-tag">
	                <input type="hidden" name="lng" id="lng" >
	                <input type="hidden" name="lat" id="lat" >
	            </div>
	            <div class="col-md-12 text-center">
	                <input type=button class="btn btn-primary" value="Ambil Foto" onClick="take_snapshot()">
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Absen</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </form>
    </div>
  </div>
</div>

<!-- Modal Absen Pulang -->
<div id="modalAbsenPulang" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form id="form-absen-pulang" action="<?= base_url('absensi/absen/pulang') ?>" method="post">
	      <div class="modal-header">
	        <h5 class="modal-title">Absen Pulang</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	            <div class="col-md-3">
	                <center>
	                	<div id="my_camera_pulang" class="col-md-3"></div>
	                </center>
	                <input type="hidden" name="userfile" class="image-tag">
	                <input type="hidden" name="lng" id="lng-pulang">
	                <input type="hidden" name="lat" id="lat-pulang">
	            </div>
	            <div class="col-md-12 text-center">
	                <input type=button class="btn btn-primary" value="Ambil Foto" onClick="take_snapshotPulang()">
	            </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Absen</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </form>
    </div>
  </div>
</div>
