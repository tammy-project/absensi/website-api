<div class="row mb-2">
    <h4 class="col-xs-12 col-sm-6 mt-0">Detail Absen</h4>
    <div class="col-xs-12 col-sm-6 ml-auto text-right">
        <form action="" method="get">
            <div class="row">
                <div class="col">
                    <select name="bulan" id="bulan" class="form-control">
                        <option value="" disabled selected>-- Pilih Bulan --</option>
                        <?php foreach($all_bulan as $bn => $bt): ?>
                            <option value="<?= $bn ?>" <?= ($bn == $bulan) ? 'selected' : '' ?>><?= $bt ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col ">
                    <select name="tahun" id="tahun" class="form-control">
                        <option value="" disabled selected>-- Pilih Tahun</option>
                        <?php for($i = date('Y'); $i >= (date('Y') - 5); $i--): ?>
                            <option value="<?= $i ?>" <?= ($i == $tahun) ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="col ">
                    <button type="submit" class="btn btn-primary btn-fill btn-block">Tampilkan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header border-bottom">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <table class="table border-0">
                            <tr>
                                <th class="border-0 py-0">Nama</th>
                                <th class="border-0 py-0">:</th>
                                <th class="border-0 py-0"><?= $karyawan->nama ?></th>
                            </tr>
                            <tr>
                                <th class="border-0 py-0">Divisi</th>
                                <th class="border-0 py-0">:</th>
                                <th class="border-0 py-0"><?= $karyawan->nama_divisi ?></th>
                            </tr>
                            <tr>
                                <th class="border-0 py-0">Shift</th>
                                <th class="border-0 py-0">:</th>
                                <th class="border-0 py-0"><?= $karyawan->nama_shift ?></th>
                            </tr>
                        </table>
                    </div>
                    <?php if(is_level('Manager')): ?>
                        <div class="col-xs-12 col-sm-6 ml-auto text-right mb-2">
		                        <div class="dropdown d-inline">
		                            <button class="btn btn-secondary dropdown-toggle" type="button" id="droprop-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                                <i class="fa fa-print"></i>
		                                Export Laporan
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="droprop-action">
		                                <a href="<?= base_url('absensi/export_pdf/' . $this->uri->segment(3) . "?bulan=$bulan&tahun=$tahun") ?>" class="dropdown-item" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a>
		                                <a href="<?= base_url('absensi/export_excel/' . $this->uri->segment(3) . "?bulan=$bulan&tahun=$tahun") ?>" class="dropdown-item" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    <?php endif; ?>
            </div>            
            <div class="card-body">
                <h4 class="card-title mb-4">Absen Bulan : <?= bulan($bulan) . ' ' . $tahun ?></h4>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                        	<th rowspan="2">No</th>
	                        <th rowspan="2">Tanggal</th>
	                        <th colspan="2"><center>Absen Masuk</center></th>
	                        <th colspan="2"><center>Absen Pulang</center></th>
                        </tr>
                        <tr>
                        	<th>Jam</th>
                        	<th>Lokasi</th>
                        	<th>Jam</th>
                        	<th>Lokasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($absen): ?>
                            <?php foreach($hari as $i => $h): ?>
                                <?php
                                    $absen_harian = array_search($h['tgl'], array_column($absen, 'tgl')) !== false ? $absen[array_search($h['tgl'], array_column($absen, 'tgl'))] : '';
                                ?>
                                <tr <?= (in_array($h['hari'], ['Minggu'])) ? 'class="bg-dark text-white"' : '' ?> <?= ($absen_harian == '') ? 'class="bg-danger text-white"' : '' ?>>
                                    <td><?= ($i+1) ?></td>
                                    <td><?= $h['hari'] . ', ' . $h['tgl'] ?></td>
                                    <td><?= is_weekend($h['tgl']) ? 'Libur Akhir Pekan' : check_wkatu_absensi(@$absen_harian['jam_masuk'], 'Masuk', $karyawan->shift_id) ?> <?= empty($absen_harian['lokasi_absen_masuk']) ? '' : '<a href=\''.base_url('uploads/absensi/').$absen_harian['foto_masuk'].'\' target=\'_blank\' class=\'badge badge-primary\'><i class=\'nc-icon nc-zoom-split\'></i></a>' ?></td>
                                    <td><?= empty($absen_harian['lokasi_absen_masuk']) ? '' : $absen_harian['lokasi_absen_masuk'] ?></td>
                                    <td><?= is_weekend($h['tgl']) ? 'Libur Akhir Pekan' : check_wkatu_absensi(@$absen_harian['jam_pulang'], 'Pulang', $karyawan->shift_id) ?> <?= empty($absen_harian['lokasi_absen_pulang']) ? '' : '<a href=\''.base_url('uploads/absensi/').$absen_harian['foto_masuk'].'\' target=\'_blank\' class=\'badge badge-primary\'><i class=\'nc-icon nc-zoom-split\'></i></a>' ?></td>
                                    <td><?= empty($absen_harian['lokasi_absen_pulang']) ? '' : $absen_harian['lokasi_absen_pulang'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td class="bg-light" colspan="6">Tidak ada data absen</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
            	<div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <table class="table border-0">
                            <tr>
                                <th class="border-0 py-0">Keterangan Warna :</th>
                            </tr>
                            <tr>
                                <td class="border-0 py-0"><span class="badge badge-danger">Terlambat</span></td>
                            </tr>
                            <tr>
                                <td class="border-0 py-0"><span class="badge badge-success">Lembur</span></td>
                            </tr>
                            <tr>
                                <td class="border-0 py-0"><span class="badge badge-primary">Normal</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


