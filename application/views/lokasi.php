<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title float-left">Lokasi Absensi</h4>
				<div class="d-inline ml-auto float-right">
					<a href="#" class="btn btn-success btn-sm btn-add-lokasi" data-toggle="modal" data-target="#modal-add-lokasi"><i class="fa fa-plus"></i> Tambah Lokasi</a>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<th>No.</th>
							<th>Lokasi</th>
							<th>Aksi</th>
						</thead>
						<tbody id="tbody-lokasi">
							<?php foreach($lokasi as $i => $d): ?>
								<tr id="<?= 'lokasi-' . $d->id_lokasi ?>">
									<td><?= ($i+1) ?></td>
									<td class="nama-lokasi"><?= $d->nama_lokasi ?></td>
									<td>
										<a href="#" class="btn btn-primary btn-sm btn-edit-lokasi" data-toggle="modal" data-target="#modal-edit-lokasi" data-lokasi="<?= base64_encode(json_encode($d)) ?>"><i class="fa fa-edit"></i> Edit</a> 
										<a href="<?= base_url('lokasi/delete/' . $d->id_lokasi) ?>" class="btn btn-danger btn-sm btn-delete ml-2" onclick="return false"><i class="fa fa-trash"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-add-lokasi" tabindex="-1" role="dialog" aria-labelledby="modal-add-lokasi-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-add-lokasi" action="<?= base_url('lokasi/add') ?>" method="post" onsubmit="return false">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-add-lokasi-label">Tambah Lokasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="nama-lokasi">Nama Lokasi :</label>
						<input type="text" name="nama_lokasi" id="nama-lokasi" class="form-control" placeholder="Nama Lokasi" required="reuired" />
					</div>
					<div class="form-group">
						<label for="map-canvas">Tandai Lokasi Pada Maps :</label>

						<div id="map-canvas" style="height: 400px;width: 100%;" class="form-control"></div>
					<input type="hidden" id="lat" name="lat">
					<input type="hidden" id="lng" name="lng">
					<input type="hidden" id="radius" name="radius">
					</div>

					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-edit-lokasi" tabindex="-1" role="dialog" aria-labelledby="modal-edit-lokasi-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-edit-lokasi" action="<?= base_url('lokasi/update') ?>" method="post" onsubmit="return false">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-add-lokasi-label">Ubah Lokasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="nama-lokasi">Nama Lokasi :</label>
						<input type="hidden" name="id_lokasi" id="edit-id-lokasi">
						<input type="text" name="nama_lokasi" id="edit-nama-lokasi" class="form-control" placeholder="Nama Lokasi" required="reuired" />
					</div>
					<div class="form-group">
						<label for="edit-map-canvas">Tandai Lokasi Pada Maps :</label>

						<div id="edit-map-canvas" style="height: 400px;width: 100%;" class="form-control"></div>
					<input type="text" id="edit-lat" name="lat">
					<input type="text" id="edit-lng" name="lng">
					<input type="text" id="edit-radius" name="radius">
					</div>

					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
