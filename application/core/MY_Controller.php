<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lokasi_model','lokasi');
	}

	// melakukan check jarak
	public function checkDistance($latitude,$longtitude){

		$allLocation = $this->lokasi->get_all();
		$result['status'] = false;
		foreach ($allLocation as $data) {
			$earth_radius = 6371;
			$latitudeUser = floatval($latitude);
			$longitudeUser = floatval($longtitude);

			$latitudeOffice = floatval($data->lat);
			$longitudeOffice = floatval($data->lng);

			$dLat = deg2rad($latitudeOffice - $latitudeUser);  
			$dLon = deg2rad($longitudeOffice - $longitudeUser); 

			$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitudeUser)) * cos(deg2rad($latitudeOffice)) * sin($dLon/2) * sin($dLon/2);  
			$c = 2 * asin(sqrt($a));  
			$d = $earth_radius * $c;

			// convert to meter
			$radius = floatval($data->radius)/1000;
			if($d<$radius){
				$result['status'] = true;
				$result['location'] = $data->nama_lokasi;
				break;
			}			
		}
		return $result;
	}

	public function saveBase64ImagePng($base64)
	{
		$base64 = str_replace('data:image/jpeg;base64,', '',$base64);
$base64 = str_replace(' ', '+', $base64);
		$image = base64_decode($base64);// decoding base64 string value
		$image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
		$filename = $image_name . '.' . 'jpg';
		//rename file name with random number
		$path = "./uploads/absensi/";
		//image uploading folder path
		$output['status'] = file_put_contents($path . $filename, $image);
		$output['name'] = $filename;
		// image is bind and upload to respective folder
		return $output;
	}


}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
