-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 21, 2020 at 12:55 AM
-- Server version: 10.3.24-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `externa1_absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absen` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `waktu` time NOT NULL,
  `keterangan` enum('Masuk','Pulang') NOT NULL,
  `lokasi_absen` varchar(255) DEFAULT NULL,
  `foto_url` text NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absen`, `tgl`, `waktu`, `keterangan`, `lokasi_absen`, `foto_url`, `id_user`) VALUES
(4, '2019-07-25', '07:21:53', 'Masuk', NULL, '', 6),
(5, '2019-07-26', '09:00:47', 'Masuk', NULL, '', 6),
(6, '2019-07-26', '16:01:03', 'Pulang', NULL, '', 6),
(7, '2019-07-25', '17:01:28', 'Pulang', NULL, '', 6),
(8, '2020-09-15', '09:45:56', 'Masuk', NULL, '', 7),
(9, '2020-09-15', '09:46:47', 'Pulang', NULL, '', 7),
(10, '2020-09-15', '09:47:26', 'Masuk', NULL, '', 7),
(11, '2020-09-15', '09:47:30', 'Pulang', NULL, '', 7),
(12, '2020-09-15', '10:30:57', 'Pulang', NULL, '', 15),
(13, '2020-09-15', '10:31:29', 'Masuk', NULL, '', 15),
(14, '2020-09-15', '10:31:42', 'Masuk', NULL, '', 15),
(15, '2020-09-15', '10:52:52', 'Masuk', NULL, '', 15),
(16, '2020-09-15', '10:53:19', 'Masuk', NULL, '', 15),
(17, '2020-09-15', '14:30:37', 'Masuk', NULL, '', 16),
(18, '2020-09-15', '14:30:50', 'Masuk', NULL, '', 16),
(19, '2020-09-15', '16:30:57', 'Pulang', NULL, '', 16),
(20, '2020-09-15', '16:31:35', 'Pulang', NULL, '', 18),
(21, '2020-09-15', '16:31:54', 'Pulang', NULL, '', 18),
(22, '2020-09-15', '16:32:07', 'Masuk', NULL, '', 18),
(23, '2020-09-15', '16:32:20', 'Pulang', NULL, '', 18),
(24, '2020-09-16', '07:36:12', 'Masuk', NULL, '', 16),
(25, '2020-09-16', '09:18:54', 'Masuk', NULL, '', 17),
(26, '2020-09-16', '09:42:54', 'Masuk', NULL, '', 17),
(27, '2020-09-16', '09:43:03', 'Masuk', NULL, '', 17),
(28, '2020-09-16', '12:52:58', 'Masuk', NULL, '', 16),
(29, '2020-09-16', '12:52:59', 'Masuk', NULL, '', 16),
(30, '2020-09-16', '12:53:12', 'Masuk', NULL, '', 16),
(31, '2020-09-16', '12:53:25', 'Pulang', NULL, '', 16),
(32, '2020-09-16', '12:54:25', 'Pulang', NULL, '', 16),
(33, '2020-09-16', '12:54:00', 'Masuk', NULL, '', 17),
(34, '2020-09-16', '16:17:26', 'Pulang', NULL, '', 16),
(35, '2020-09-16', '16:13:26', 'Pulang', NULL, '', 16),
(36, '2020-09-17', '07:51:21', 'Masuk', NULL, '', 16),
(72, '2020-10-12', '13:51:21', 'Masuk', 'test123', '4811bac12c3daf9f694a7505791fc0e9.jpg', 16),
(73, '2020-10-12', '16:34:41', 'Pulang', 'Office REKA Jl.Candi Sewu No.30', 'af87e7a49b576bfd6e1abca736ae6d50.jpg', 16),
(74, '2020-10-13', '08:11:47', 'Masuk', NULL, '810edb19999dcb5a00db40c23a5d3da2.jpg', 16),
(75, '2020-10-13', '08:12:06', 'Pulang', NULL, '1c7a15854689c39734223bd4aeeb5464.jpg', 16),
(76, '2020-10-13', '08:12:25', 'Pulang', NULL, '7158287da3a3d8fb8de1686ad8808ffe.jpg', 16),
(77, '2020-10-13', '08:12:50', 'Pulang', NULL, '235cb91408ad55e29a5b0b2164514d2a.jpg', 16),
(78, '2020-10-13', '08:16:07', 'Masuk', NULL, 'f492d23346e5b0b3c298b2c918b7c1e7.jpg', 17),
(79, '2020-10-14', '15:04:23', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '04eab958bdcc4045c5b472cd7f612a42.jpg', 16),
(80, '2020-10-14', '16:26:16', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '2fd7a53ed6294c6e9fc3156d8c3a52ca.jpg', 17),
(81, '2020-10-14', '16:32:46', 'Pulang', 'Office REKA Jl.Candi Sewu No.30', '5dd581c811c193625d6737db8aa40391.jpg', 17),
(82, '2020-10-15', '08:25:49', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '2b6db4416d78ca70dc06682cbd2e24c3.jpg', 17),
(83, '2020-10-15', '08:33:29', 'Masuk', 'dummy jkt', '2e73f7be38cffa759384ea5a57434804.jpg', 19),
(84, '2020-10-15', '11:56:48', 'Masuk', NULL, 'fa13662d41973deb47a8148bf324ff9e.jpg', 16),
(85, '2020-10-15', '11:57:04', 'Pulang', NULL, 'd06601d05c17505f95a4bb356088dff7.jpg', 16),
(86, '2020-10-16', '13:36:33', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', 'd135ea1ceeb14fda299bc26cfe407af8.jpg', 16),
(87, '2020-10-16', '16:11:06', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '092441a6e1f7a6867880b3b1b6187447.jpg', 17),
(88, '2020-10-19', '07:57:49', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '8951c92a8e8485dcdfeab4ebd8697360.jpg', 16),
(89, '2020-10-19', '09:20:22', 'Masuk', 'Office REKA Jl.Candi Sewu No.30', '4eaf4b18fcd7f12a864761d5c1fa3494.jpg', 17);

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id_divisi` smallint(3) NOT NULL,
  `nama_divisi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id_divisi`, `nama_divisi`) VALUES
(1, 'Information Technology'),
(2, 'Human Resource & GA'),
(3, 'Finance & Tax'),
(4, 'Corporate Secretary'),
(5, 'Finance & HR Director'),
(6, 'General Affairs'),
(7, 'Mechanical Engineering'),
(8, 'Electrical Engineering'),
(9, 'Finance'),
(10, 'Accounting'),
(11, 'Bidding & Pricing'),
(12, 'Assembling'),
(13, 'Procurement'),
(14, 'Cleaning Service'),
(15, 'Security');

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE `jam` (
  `id_jam` tinyint(1) NOT NULL,
  `start` time NOT NULL,
  `finish` time NOT NULL,
  `keterangan` enum('Masuk','Lembur') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `start`, `finish`, `keterangan`) VALUES
(1, '03:16:00', '03:16:01', 'Masuk'),
(2, '03:19:00', '21:00:00', 'Lembur');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `nama_lokasi` varchar(255) NOT NULL,
  `lng` text NOT NULL,
  `lat` text NOT NULL,
  `radius` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_lokasi`, `lng`, `lat`, `radius`) VALUES
(1, 'Office REKA Jl.Candi Sewu No.30', '111.52146595423086', '-7.616280285486825', '69.18960530835857'),
(3, 'test3', '111.52116163002471', '-7.617130630948014', '71.55706729925478'),
(9, 'dummy jkt', '106.88821097176674', '-6.145512349643262', '9451.721302407477'),
(11, 'Workshop Sukosari REKA ', '111.53443648727783', '-7.6108092167936325', '254.6781537436738'),
(12, 'PT INKA (Persero)', '111.52341842651367', '-7.6175914808896135', '331.99046719541104');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id_shift` int(11) NOT NULL,
  `nama_shift` varchar(255) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id_shift`, `nama_shift`, `jam_mulai`, `jam_selesai`, `created_at`) VALUES
(3, 'Kelompok 2', '07:28:00', '09:00:00', '2020-09-26 10:30:44'),
(6, 'Kelompok 1', '07:30:00', '16:30:00', '2020-10-14 11:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` smallint(5) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(20) DEFAULT 'no-foto.png',
  `divisi` smallint(5) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `level` enum('Manager','Karyawan') NOT NULL DEFAULT 'Karyawan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nik`, `nama`, `telp`, `email`, `foto`, `divisi`, `shift_id`, `username`, `password`, `level`) VALUES
(1, '', 'Admin IT', '08139212092', 'itsupport@ptrekaindo.co.id', '1600140102.PNG', NULL, NULL, 'admin', '$2y$10$1pK8bg/11vcbiKvXszspp.OuSJJzXBsEaAST1IRw3UHPnK1qHjeX.', 'Manager'),
(8, '8931289124891', 'Manager 1', '', '', 'no-foto.png', NULL, NULL, 'manager_1', '$2y$10$XtMY01KEOd5I065s8Exs0OcQ373RvRNG1JznORr6TmmBNWnZ3vjjK', 'Manager'),
(9, '1231231238900', 'Manager 2', '', '', 'no-foto.png', NULL, NULL, 'manager_2', '$2y$10$iJWUOXDznGEmxo.bqnhtmeFL51jN5130LfDlKg8VROfoEmlgC.cFW', 'Manager'),
(10, '908121310291', 'Manager 3', '', '', 'no-foto.png', NULL, NULL, 'manager_3', '$2y$10$uGsLvgl.6ji2iZ7tWkNvPelTwZdLQ6QA81Yawa20wsLairCXqV8BO', 'Manager'),
(11, '123801204012', 'Manager 4', '', '', 'no-foto.png', NULL, NULL, 'master_4', '$2y$10$Kot81WNqrho4WlcYI13kT.Y5V2sMg1ZSAXcITrp8cj3dqHpbl4vrS', 'Manager'),
(16, 'REKA01-1910.032', 'Tamara Dhea Pramesti', '0895412353733', 'tamara@ptrekaindo.co.id', 'no-foto.png', 1, 6, 'tamara', '$2y$10$7pIe4L.90GZ.d4oqlW1h2.q7lh/Hj/g5y1DmW.NWaOptjDA8V1.0q', 'Karyawan'),
(17, 'REKA01-0117.033', 'Rizky Ryan Raditya', '081234918655', 'rizky@ptrekaindo.co.id', 'no-foto.png', 1, 6, 'rizky', '$2y$10$.z1Q80eA7Dmoac2AsubVMeAQ8phwarcay4VFemZQj.39Z1iIO4di6', 'Karyawan'),
(18, 'REKA01-1001', 'testKaryawan', '085732269397', 'test@ptrekaindo.co.id', 'no-foto.png', 2, 3, 'testKaryawan', '$2y$10$M5ndax2eeTRDtMH2Zgq5zuX7XZwtN.mCLYMO6iZzAeRJLjdhmeACW', 'Karyawan'),
(19, 'test123', 'Andri', '1234525', 'andri.polinema@gmail.com', 'no-foto.png', 1, 3, 'andri', '$2y$10$L4FRQ8euFnQhUErVmdr8GuCGcHAA/UJKc5JpB6VbXIXW9mlQ1HcqW', 'Karyawan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id_shift`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `id_divisi` smallint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id_shift` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
