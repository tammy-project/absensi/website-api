$(document).ready(function() {
    let sendAjax = function(url, formData, type = 'POST') {
        let dataReturn = [];
        $.ajax({
            url: url,
            type: type,
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            async: false,
            success: (res) => {
                console.log(res);
                let status = (res.status == 'error') ? 'danger' : res.status;
                let alertTemplate = `
                    <div class="alert alert-${status} alert-dismissible fade show" role="alert">
                        ${res.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                `;
                $('#alert').html(alertTemplate);
                if (typeof res.data !== 'undefined') {
                    dataReturn = res.data;
                }
            },
            error: (err) => {
                console.log(err);
            }
        });
        $('input, textarea, select').val('');

        return dataReturn;
    }

    if (typeof absenNeeded !== 'undefined') {
        let absenProperty = $.parseJSON(absenNeeded);
        swal({
            title: 'Yuk Absen!',
			text: absenProperty.message,
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Oke Absen',
                    className : 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    text: 'Nanti saja',
                    className: 'btn btn-danger'
                }
            }
        }).then((isDelete) => {
            if (isDelete) {
                window.href.location = absenProperty.href;
            } else {
                swal.close();
            }
        });
        return false;
    }

    $('.custom-file-input').change(function(e) {
        let filename = e.target.files[0].name;
        console.log($(this).closest('.custom-file-label'));
        $(this).next().text(filename);
        console.log(filename);
    });

    $('.btn-edit-jam').click(function() {
        let dataEncode = atob($(this).attr('data-jam'));
        let dataJam = $.parseJSON(dataEncode);
        
        $('#edit-keterangan').text(dataJam.keterangan);
        $('#edit-id-jam').val(dataJam.id_jam);
        $('#edit-start').val(dataJam.start);
        $('#edit-finish').val(dataJam.finish);
    });

    $('#form-edit-jam').submit(function() {
        let form = document.getElementById('form-edit-jam');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        let targetParent = $('#jam-' + dataReturn.id_jam);
        targetParent.find('.jam-start').text(dataReturn.start);
        targetParent.find('.jam-finish').text(dataReturn.finish);
        targetParent.find('a.btn-edit-jam').attr('data-jam', btoa(JSON.stringify(dataReturn)));

        $('#edit-jam').modal('hide');
    });

    $('#form-add-divisi').submit(function() {
        let form = document.getElementById('form-add-divisi');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        let target = $('#tbody-divisi');
        let rowCount = parseInt(target.children('tr').length) + 1;
        let templateRow = `
            <tr id="divisi-${dataReturn.id_divisi}">
                <td>${rowCount}</td>
                <td class="nama-divisi">${dataReturn.nama_divisi}</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm btn-edit-divisi" data-toggle="modal" data-target="#modal-edit-divisi" data-divisi="${btoa(JSON.stringify(dataReturn))}"><i class="fa fa-edit"></i> Edit</a> 
                    <a href="${BASEURL + 'divisi/destroy/' + dataReturn.id_divisi}" class="btn btn-danger btn-sm btn-delete ml-2" onclick="return false"><i class="fa fa-trash"></i> Hapus</a>
                </td>
            </tr>
        `;
        target.append(templateRow);
        $('#modal-add-divisi').modal('hide');
    });

    $('body').on('click', '.btn-edit-divisi', function() {
        let dataEncode = atob($(this).attr('data-divisi'));
        console.log($(this).attr('data-divisi'));
        console.log(dataEncode);
        let dataDivisi = $.parseJSON(dataEncode);

        $('#edit-id-divisi').val(dataDivisi.id_divisi);
        $('#edit-nama-divisi').val(dataDivisi.nama_divisi);
    });

    $('#form-edit-divisi').submit(function() {
        let form = document.getElementById('form-edit-divisi');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        console.log(dataReturn);
        let targetParent = $('tr#divisi-' + dataReturn.id_divisi);
        targetParent.find('.nama-divisi').text(dataReturn.nama_divisi);
        $('#modal-edit-divisi').modal('hide');
    });

    // shift start
    $('#form-add-shift').submit(function() {
        let form = document.getElementById('form-add-shift');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        let target = $('#tbody-shift');
        let rowCount = parseInt(target.children('tr').length) + 1;
        let templateRow = `
            <tr id="shift-${dataReturn.id_shift}">
                <td>${rowCount}</td>
                <td class="nama-shift">${dataReturn.nama_shift}</td>
                <td class="jam-mulai">${dataReturn.jam_mulai}</td>
                <td class="jam-selesai">${dataReturn.jam_selesai}</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm btn-edit-shift" data-toggle="modal" data-target="#modal-edit-shift" data-shift="${btoa(JSON.stringify(dataReturn))}"><i class="fa fa-edit"></i> Edit</a> 
                    <a href="${BASEURL + 'shift/destroy/' + dataReturn.id_shift}" class="btn btn-danger btn-sm btn-delete ml-2" onclick="return false"><i class="fa fa-trash"></i> Hapus</a>
                </td>
            </tr>
        `;
        target.append(templateRow);
        $('#modal-add-shift').modal('hide');
    });

    $('body').on('click', '.btn-edit-shift', function() {
        let dataEncode = atob($(this).attr('data-shift'));
        console.log($(this).attr('data-shift'));
        console.log(dataEncode);
        let dataShift = $.parseJSON(dataEncode);

        $('#edit-id-shift').val(dataShift.id_shift);
        $('#edit-nama-shift').val(dataShift.nama_shift);
        $('#edit-jam-mulai').val(dataShift.jam_mulai);
        $('#edit-jam-selesai').val(dataShift.jam_selesai);
    });

    $('#form-edit-shift').submit(function() {
        let form = document.getElementById('form-edit-shift');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        console.log(dataReturn);
        let targetParent = $('tr#shift-' + dataReturn.id_shift);
        targetParent.find('.nama-shift').text(dataReturn.nama_shift);
        targetParent.find('.jam-mulai').text(dataReturn.jam_mulai);
        targetParent.find('.jam-selesai').text(dataReturn.jam_selesai);
        $('#modal-edit-shift').modal('hide');
    });
    // shift end

    $('#form-add-lokasi').submit(function() {
        let form = document.getElementById('form-add-lokasi');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        let target = $('#tbody-lokasi');
        let rowCount = parseInt(target.children('tr').length) + 1;
        let templateRow = `
            <tr id="lokasi-${dataReturn.id_lokasi}">
                <td>${rowCount}</td>
                <td class="nama-lokasi">${dataReturn.nama_lokasi}</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm btn-edit-lokasi" data-toggle="modal" data-target="#modal-edit-lokasi" data-lokasi="${btoa(JSON.stringify(dataReturn))}"><i class="fa fa-edit"></i> Edit</a> 
                    <a href="${BASEURL + 'lokasi/delete/' + dataReturn.id_lokasi}" class="btn btn-danger btn-sm btn-delete ml-2" onclick="return false"><i class="fa fa-trash"></i> Hapus</a>
                </td>
            </tr>
        `;
        target.append(templateRow);
        $('#modal-add-lokasi').modal('hide');
    });

    $('body').on('click', '.btn-edit-lokasi', function() {
        let dataEncode = atob($(this).attr('data-lokasi'));
        let dataLokasi = $.parseJSON(dataEncode);

        $('#edit-id-lokasi').val(dataLokasi.id_lokasi);
        $('#edit-nama-lokasi').val(dataLokasi.nama_lokasi);
        $('#edit-lng').val(dataLokasi.lng);
        $('#edit-lat').val(dataLokasi.lat);
        $('#edit-radius').val(dataLokasi.radius);

        var editMapCanvas = document.getElementById("edit-map-canvas");
        var myCenter= new google.maps.LatLng(dataLokasi.lat,dataLokasi.lng);
        var mapOptions = {
				center: myCenter, zoom: 17,
				mapTypeId:'terrain'
		};
        var map2 = new google.maps.Map(editMapCanvas, mapOptions);
        google.maps.event.addListener(map2, 'click', function(event) {
				placeMarkerEdit(map2, event.latLng);
			});
    });

    $('#form-edit-lokasi').submit(function() {
        let form = document.getElementById('form-edit-lokasi');
        let url = $(this).attr('action');
        let data = new FormData(form);

        let dataReturn = sendAjax(url, data);
        console.log(dataReturn);
        let targetParent = $('tr#lokasi-' + dataReturn.id_lokasi);
        targetParent.find('.nama-lokasi').text(dataReturn.nama_lokasi);
        $('#modal-edit-lokasi').modal('hide');
    });

    $('body').on('click', '.btn-delete', function() {
        let destionation = $(this).attr('href');
        
        swal({
            title: 'Yakin Hapus?',
			text: "Data yang dihapus akan hilang permanen!",
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Ya Hapus!',
                    className : 'btn btn-danger'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-primary'
                }
            }
        }).then((isDelete) => {
            if (isDelete) {
                sendAjax(destionation, [], 'GET');
                $(this).closest('tr').remove();
            } else {
                swal.close();
            }
        });
        return false;
    });
});


