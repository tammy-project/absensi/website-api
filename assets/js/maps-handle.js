// var map;
var marker;
var infowindow;
var circle;
function initMap() {
	var mapCanvas = document.getElementById("map-canvas");
	var editMapCanvas = document.getElementById("edit-map-canvas");
	var myCenter= new google.maps.LatLng(-7.6162207,111.5213923);
	var mapOptions = {
		center: myCenter, zoom: 17,
		mapTypeId:'terrain'
	};
	var map = new google.maps.Map(mapCanvas, mapOptions);
	google.maps.event.addListener(map, 'click', function(event) {
		placeMarker(map, event.latLng);
	});
}
function placeMarker(map, location) {
	if (!marker || !marker.setPosition) {
		marker = new google.maps.Marker({
			position: location,
			map: map,
			animation: google.maps.Animation.BOUNCE,
		});
	} else {
		marker.setPosition(location);
	}
	if (!!infowindow && !!infowindow.close) {
		infowindow.close();
	}
	infowindow = new google.maps.InfoWindow({
		content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
	});
	infowindow.open(map,marker);
	var geocoder = new google.maps.Geocoder;
	geocoder.geocode({'location': location}, function(results, status) {
		if (status === 'OK') {
			if (results[0]) {
				var latlng = marker.getPosition();
				document.getElementById("lat").value = latlng.lat();
				document.getElementById("lng").value = latlng.lng();
			} else {
				window.alert('No results found');
			}
		} else {
			window.alert('Geocoder failed due to: ' + status);
		}
	});
	if(circle){
		circle.setMap(null);
	}
	let radius = document.getElementById("radius").value;
	if(radius===""){
		radius = 10;
	}else{
		radius = parseInt(radius)
	}
	circle = new google.maps.Circle({
		map: map,
		fillColor : '#BBD8E9',
		fillOpacity : 0.3,
		radius : radius,
		strokeColor : '#BBD8E9',
		strokeOpacity : 0.9,
		strokeWeight : 2,
		editable: true
	});
	google.maps.event.addListener(circle, 'radius_changed', function () {
		console.log("radius: "+circle.getRadius());
		document.getElementById("radius").value = circle.getRadius(); 
	});
	circle.bindTo('center', marker, 'position');
}

function placeMarkerEdit(map, location) {
	if (!marker || !marker.setPosition) {
		marker = new google.maps.Marker({
			position: location,
			map: map,
			animation: google.maps.Animation.BOUNCE,
		});
	} else {
		marker.setPosition(location);
	}
	if (!!infowindow && !!infowindow.close) {
		infowindow.close();
	}
	infowindow = new google.maps.InfoWindow({
		content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
	});
	infowindow.open(map,marker);
	var geocoder = new google.maps.Geocoder;
	geocoder.geocode({'location': location}, function(results, status) {
		if (status === 'OK') {
			if (results[0]) {
				var latlng = marker.getPosition();
				document.getElementById("edit-lat").value = latlng.lat();
				document.getElementById("edit-lng").value = latlng.lng();
			} else {
				window.alert('No results found');
			}
		} else {
			window.alert('Geocoder failed due to: ' + status);
		}
	});
	if(circle){
		circle.setMap(null);
	}
	let radius = document.getElementById("edit-radius").value;
	if(radius===""){
		radius = 10;
	}else{
		radius = parseInt(radius)
	}
	circle = new google.maps.Circle({
		map: map,
		fillColor : '#BBD8E9',
		fillOpacity : 0.3,
		radius : radius,
		strokeColor : '#BBD8E9',
		strokeOpacity : 0.9,
		strokeWeight : 2,
		editable: true
	});
	google.maps.event.addListener(circle, 'radius_changed', function () {
		console.log("radius: "+circle.getRadius());
		document.getElementById("radius").value = circle.getRadius(); 
	});
	circle.bindTo('center', marker, 'position');
}
