var url;	
function absen() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
	} else {
		alert("Geolocation tidak support untuk browser ini, mohon mengganti browser untuk melanjutkan!");
	}
}

function successCallback(position) {
	$("#lat").val(position.coords.latitude);
	$("#lng").val(position.coords.longitude);
	$("#lat-pulang").val(position.coords.latitude);
	$("#lng-pulang").val(position.coords.longitude);
}

function errorCallback(error) {
	let response;
	switch(error.code) {
		case error.PERMISSION_DENIED:
		response = "Refresh browser dan Ijinkan mengambil lokasi untuk melanjutkan absen."
		break;
		case error.POSITION_UNAVAILABLE:
		response = "Informasi lokasi tidak tersedia."
		break;
		case error.TIMEOUT:
		response = "Terjadi kesalahan, terlalu lama mengambil lokasi."
		break;
		case error.UNKNOWN_ERROR:
		response = "Terjadi kesalahan yang tidak diketahui."
		break;
	}
	alert(response);
}

$('#exampleModal').on('hidden.bs.modal', function () {
	Webcam.reset();
});

function openWeb() {
	Webcam.set({
		width: 390,
		height: 290,
		image_format: 'jpeg',
		jpeg_quality: 90

	});
	Webcam.attach('#my_camera');

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
	} else {
		alert("Geolocation tidak support untuk browser ini, mohon mengganti browser untuk melanjutkan!");
	}
}

function openWebPulang() {
	Webcam.set({
		width: 390,
		height: 290,
		image_format: 'jpeg',
		jpeg_quality: 90

	});
	Webcam.attach('#my_camera_pulang');

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
	} else {
		alert("Geolocation tidak support untuk browser ini, mohon mengganti browser untuk melanjutkan!");
	}
}

function take_snapshot() {
	Webcam.snap( function(data_uri) {
		$(".image-tag").val(data_uri);
		Webcam.reset();
		document.getElementById('my_camera').innerHTML = '<img src="'+data_uri+'"/>'
	} );
}

function take_snapshotPulang() {
	Webcam.snap( function(data_uri) {
		$(".image-tag").val(data_uri);
		Webcam.reset();
		document.getElementById('my_camera_pulang').innerHTML = '<img src="'+data_uri+'"/>'
	} );
}
